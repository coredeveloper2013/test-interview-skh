<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class ManagerLoginController extends Controller
{

    protected $redirectTo = '/manager';

 public function __construct(){
  $this->middleware('guest:manager', ['except' => ['logout']]);
 }


    public function showLoginForm(){
     return view('auth.manager-login');
    }

    public function login(Request $request){
     
     //Validate the form data
     $this->validate($request, [
      'email'   => 'email|required',
      'password'   => 'required|min:6'
      ]);

     //Attempt to log the user in
     if (Auth::guard('manager')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
      //If successful then redirect to the intended location
    //   return redirect()->intended(route('manager.dashboard'));
        return redirect('/manager');
      
     }

     //If unsuccessfull, then redirect to the admin login with the data
     return redirect()->back()->withInput($request->only('email', 'remember'));
    }


    public function logout()
    {
        Auth::guard('manager')->logout();
        return redirect()->route('manager.login');
    }
}