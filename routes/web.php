<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

// Auth::routes();

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');

Route::get('/home', 'HomeController@index')->name('home');




Route::group(['prefix' => 'admin'], function() {
	
	// Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
	// Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

	Route::get('/','AdminController@index');
});

Route::group(['prefix' => 'manager'], function() {
	
    // Route::get('/login', 'Auth\ManagerLoginController@showLoginForm')->name('manager.login');
    // Route::post('/login', 'Auth\ManagerLoginController@login')->name('manager.login.submit');
    Route::post('/logout', 'Auth\ManagerLoginController@logout')->name('manager.logout');
   
    Route::get('/','ManagerController@index');
});
